#version 330 core
in vec2 texCoord;

out vec4 FragColor;

uniform int texChannels;
uniform sampler2D ourTexture;
uniform vec3 ourColor = vec3(1.0, 1.0, 1.0);

void main()
{
	if (texChannels == 1)
	{
		float red = texture(ourTexture, texCoord).r;
		FragColor = vec4(red, red, red, 1.0);
	}
	else
	{
		FragColor = texture(ourTexture, texCoord) * vec4(ourColor, 1.0);
	}
}
