#version 330 core
in vec2 TexCoords;
out vec4 colour;

uniform sampler2D text;
uniform vec4 textColour;

void main()
{
	colour = vec4(textColour.x, textColour.y, textColour.z, texture(text, TexCoords).r * textColour.w);
}
