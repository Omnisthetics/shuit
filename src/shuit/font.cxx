#include "./font.h"
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <bitset>

Font::Font(std::string filepath, int newSize, Shader * newShader)
{
	FT_Library ft;
	FT_Face face;

	//Initialise FreeType2
	if (FT_Init_FreeType(&ft) != 0)
		std::cout << "Failed to initialise FreeType2!" << std::endl;

	//Load font
	if (FT_New_Face(ft, filepath.c_str(), 0, &face) != 0)
		std::cout << "Failed to load font " << filepath << "!" << std::endl;

	//Set glyph size
	FT_Set_Pixel_Sizes(face, 0, newSize);
	size = newSize;

	//Disable byte-alignment restriction
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//Generate base ASCII glyphs
	for (int i = 0; i < 10000; i++)
	{
		int glyphIndex = FT_Get_Char_Index(face, i);

		if (glyphIndex == 0)
			continue;

		if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_RENDER) != 0)
		{
			std::cout << "Failed to load glyph '" << glyphIndex << "'!" << std::endl;
			continue;
		}

		//Generate glyph texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, face->glyph->bitmap.width, face->glyph->bitmap.rows,
				0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

		//Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		//Add glyph to character map
		Character character = {texture, face->glyph->bitmap.width, face->glyph->bitmap.rows,
				face->glyph->bitmap_left, face->glyph->bitmap_top, face->glyph->advance.x};

		characters.insert(std::pair<int, Character>(i, character));
	}

	//Unbind last texture
	glBindTexture(GL_TEXTURE_2D, 0);

	//Clear FreeType2
	FT_Done_Face(face);
	FT_Done_FreeType(ft);

	//Configure VAO/VBO
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 5, NULL, GL_DYNAMIC_DRAW);

	//Enable position attributes
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

	//Enable texcoord attributes
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid *)(3 * sizeof(GLfloat)));

	//Unbind buffers
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//Assign shader
	shader = newShader;
}

Font::Font(const Font & font)
{
	VAO = font.VAO;
	VBO = font.VBO;
	shader = font.shader;
	size = font.size;
	characters = font.characters;
}

Font & Font::operator=(const Font & font)
{
	VAO = font.VAO;
	VBO = font.VBO;
	shader = font.shader;
	size = font.size;
	characters = font.characters;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

void Font::renderText(std::string text, glm::mat4 projection, glm::mat4 view, float x, float y,
		float scale, glm::vec4 colour, int alignment)
{
	//Prepare shader
	shader->use();
	shader->setMat4("projection", projection);
	shader->setMat4("view", view);
	shader->setVec4("textColour", colour);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	//Adjust starting position for chosen alignment
	if (alignment != LEFT_ALIGN)
	{
		int textWidth = getTextWidth(text);

		if (alignment == CENTER_ALIGN)
			x -= textWidth/2;
		else if (alignment == RIGHT_ALIGN)
			x -= textWidth;
	}

	for (int i = 0; i < text.size(); i++)
	{
		Character ch = characters[getCodepoint(text, i)];

		float posX = x + ch.bearingX * scale;
		float posY = y - (ch.height - ch.bearingY) * scale;
		float width = ch.width * scale;
		float height = ch.height * scale;

		GLfloat vertices[6][5] = {
			{posX, posY + height, 0.0f, 0.0f, 0.0f},
			{posX, posY, 0.0f, 0.0f, 1.0f},
			{posX + width, posY, 0.0f, 1.0f, 1.0f},

			{posX, posY + height, 0.0f, 0.0f, 0.0f},
			{posX + width, posY, 0.0f, 1.0f, 1.0f},
			{posX + width, posY + height, 0.0f, 1.0f, 0.0f}
		};

		glBindTexture(GL_TEXTURE_2D, ch.textureID);

		//Update VBO
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

		//Render glyph
		glDrawArrays(GL_TRIANGLES, 0, 6);

		//Advance cursor for next glyph
		//Advance = # of 1/64 pixels
		//Bitshift is used to calculate actual pixels (2^6 = 64)
		x += (ch.advance >> 6) * scale;
	}

	//Unbind buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

int Font::getCodepoint(std::string text, int & pos)
{
	if (text[pos] >= 0 || text[pos] < -62)  //Signed C2
	{
		return text[pos];
	}
	else if (text[pos] >= -62 && text[pos] <= -33) //Signed 0xC2 and 0xDF
	{
		std::string uniChar = text.substr(pos, 2);
		pos++;
		return get2ByteCodepoint(uniChar);
	}
	else if (text[pos] >= -32 && text[pos] <= -17) //Signed 0xE0 and 0xEF
	{
		std::string uniChar = text.substr(pos, 3);
		pos += 2;
		return get3ByteCodepoint(uniChar);
	}
	else if (text[pos] >= -16 && text[pos] <= -12) //Signed 0xF0 and 0xF4
	{
		std::string uniChar = text.substr(pos, 4);
		pos += 3;
		return get4ByteCodepoint(uniChar);
	}
	else
	{
		return -1;
	}
}

int Font::get2ByteCodepoint(std::string uniChar)
{
	char char1 = uniChar[0];
	char1 &= ~(1 << 7);
	char1 &= ~(1 << 6);
	char char2 = uniChar[1];
	char2 &= ~(1 << 7);

	int int1 = char1 << 6;
	int int2 = char2;

	return int1 + int2;
}

int Font::get3ByteCodepoint(std::string uniChar)
{
	char char1 = uniChar[0];
	char1 &= ~(1 << 7);
	char1 &= ~(1 << 6);
	char1 &= ~(1 << 5);
	char char2 = uniChar[1];
	char2 &= ~(1 << 7);
	char char3 = uniChar[2];
	char3 &= ~(1 << 7);

	int int1 = char1 << 12;
	int int2 = char2 << 6;
	int int3 = char3;

	return int1 + int2 + int3;
}

int Font::get4ByteCodepoint(std::string uniChar)
{
	char char1 = uniChar[0];
	char1 &= ~(1 << 7);
	char1 &= ~(1 << 6);
	char1 &= ~(1 << 5);
	char1 &= ~(1 << 4);
	char char2 = uniChar[1];
	char2 &= ~(1 << 7);
	char char3 = uniChar[2];
	char3 &= ~(1 << 7);
	char char4 = uniChar[3];
	char4 &= ~(1 << 7);

	int int1 = char1 << 18;
	int int2 = char2 << 12;
	int int3 = char3 << 6;
	int int4 = char4;

	return int1 + int2 + int3 + int4;
}

//================================================================================================
//Getters
//================================================================================================

int Font::getSize()
{
	return size;
}

int Font::getTextWidth(std::string text)
{
	int textWidth = 0;

	for (int i = 0; i < text.size(); i++)
	{
		Character ch = characters[getCodepoint(text, i)];

		textWidth += ch.advance >> 6;
	}

	return textWidth;
}

int Font::getTextHeight(std::string text)
{
	int textHeight = 0;

	for (int i = 0; i < text.size(); i++)
	{
		Character ch = characters[getCodepoint(text, i)];

		if (ch.height > textHeight)
			textHeight = ch.height;
	}

	return textHeight;
}

Character Font::getCharacter(std::string text)
{
	int pos = 0;
	return characters[getCodepoint(text, pos)];
}
