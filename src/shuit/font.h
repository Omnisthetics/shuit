#ifndef FONT_H_
#define FONT_H_

#include "./shader.h"
#include <map>

enum Alignments
{
	CENTER_ALIGN,
	LEFT_ALIGN,
	RIGHT_ALIGN
};

struct Character
{
	GLuint textureID;
	GLint width;
	GLint height;
	GLint bearingX;
	GLint bearingY;
	GLuint advance;
};

class Font
{
private:
	//OpenGL related members
	GLuint VAO;
	GLuint VBO;
	Shader * shader;

	//Font related members
	int size;
	std::map<int, Character> characters;
public:
	//Constructors
	Font() {};
	Font(std::string filepath, int newSize, Shader * newShader);
	Font(const Font & font);
	Font & operator=(const Font & font);

	//Misc.
	void renderText(std::string text, glm::mat4 projection, glm::mat4 view, float x, float y,
			float scale, glm::vec4 colour, int alignment);
	int getCodepoint(std::string text, int & pos);
	int get2ByteCodepoint(std::string uniChar);
	int get3ByteCodepoint(std::string uniChar);
	int get4ByteCodepoint(std::string uniChar);

	//Getters
	int getSize();
	int getTextWidth(std::string text);
	int getTextHeight(std::string text);
	Character getCharacter(std::string text);
};

#endif
