#ifndef BUTTON_H_
#define BUTTON_H_

#include "./image.h"
#include "./uiobject.h"
#include "./colourobject.h"

class Button : public UiObject
{
private:
	Image image;
	Rect imageClip;
	ColourObject border;
	ColourObject background;
public:
	//Constructors
	Button() {};
	Button(std::string imageName, GLFWwindow * window, Shader * imageShader, int newHoriMode,
			int newVertMode, float newX, float newY, float newWidth, float newHeight);
	Button(const Button & button);
	Button & operator=(const Button & button);

	//Misc.
	void render(glm::mat4 projection, glm::mat4 view);

	//Getters
	Image * getImage();

	//Setters
	void setImage(std::string imageName);
	void setImageClip(Rect newClip);
	void setBorderColour(glm::vec4 colour);
	void setBackgroundColour(glm::vec4 colour);
};

#endif
