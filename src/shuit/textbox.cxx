#include "./textbox.h"
#include "./font.h"
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

//================================================================================================
//Constructor
//================================================================================================

Textbox::Textbox(Font * newFont, GLFWwindow * window, int newHoriMode, int newVertMode,
		float newX, float newY, float newW, float newH)
		: UiObject(window, newHoriMode, newVertMode, newX, newY, newW, newH)
{
	text = "";
	font = newFont;

	active = false;

	cursorPos = 0;
	cameraPos = 0;
	paddingX = 0;
	textLimit = 0;
	cursorTime = 1;

	textColour = glm::vec4(1.0f);
	border = ColourObject(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));
	background = ColourObject(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));
}

Textbox::Textbox(const Textbox & other) : UiObject(other)
{
	font = other.font;
	active = other.active;

	text = other.text;
	textColour = other.textColour;
	textLimit = other.textLimit;
	cursorPos = other.cursorPos;
	cameraPos = other.cameraPos;
	paddingX = other.paddingX;
	cursorTime = other.cursorTime;

	border = other.border;
	background = other.background;
}

Textbox & Textbox::operator=(const Textbox & other)
{
	window = other.window;
	horiPosMode = other.horiPosMode;
	vertPosMode = other.vertPosMode;
	modX = other.modX;
	modY = other.modY;
	posX = other.posX;
	posY = other.posY;
	width = other.width;
	height = other.height;
	visible = other.visible;

	font = other.font;
	active = other.active;

	text = other.text;
	textColour = other.textColour;
	textLimit = other.textLimit;
	cursorPos = other.cursorPos;
	cameraPos = other.cameraPos;
	paddingX = other.paddingX;
	cursorTime = other.cursorTime;

	border = other.border;
	background = other.background;

	return *this;
}

//================================================================================================
//Input Management
//================================================================================================

void Textbox::processKeyboardCallback(int key, int scancode, int action, int mods)
{
	if (visible && active)
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
		{
			switch (key)
			{
				case GLFW_KEY_LEFT:
					if (cursorPos > 0)
					{
						cursorPos--;
						adjustCamera(TBOX_LEFT_RIGHT);
					}
					break;

				case GLFW_KEY_RIGHT:
					if (cursorPos < text.size())
					{
						cursorPos++;
						adjustCamera(TBOX_LEFT_RIGHT);
					}
					break;

				case GLFW_KEY_HOME:
					cursorPos = 0;
					adjustCamera(TBOX_HOME);
					break;

				case GLFW_KEY_END:
					cursorPos = text.size();
					adjustCamera(TBOX_END);
					break;

				case GLFW_KEY_BACKSPACE:
					if (cursorPos > 0)
					{
						text.erase(cursorPos-1, 1);
						cursorPos--;
						adjustCamera(TBOX_DELETE);
					}
					break;

				case GLFW_KEY_DELETE:
					if (cursorPos < text.size())
					{
						text.erase(cursorPos, 1);
						adjustCamera(TBOX_DELETE);
					}
					break;

				case GLFW_KEY_V:
					if (mods == GLFW_MOD_CONTROL)
					{
						std::string clipboardText = glfwGetClipboardString(window);
						text.insert(cursorPos, clipboardText);
						cursorPos += clipboardText.size();
						adjustCamera(TBOX_PASTE);
					}
					break;
			}
		}
	}
}

void Textbox::processMouseCallback(int button, int action, int mods)
{
	double mouseX;
	double mouseY;
	glfwGetCursorPos(window, &mouseX, &mouseY);

	int windowWidth;
	int windowHeight;
	glfwGetWindowSize(window, &windowWidth, &windowHeight);

	//Convert mouseY to standard coordinates
	mouseY = windowHeight - mouseY;

	if (visible && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		if (active && highlighted(mouseX, mouseY))
		{
			int textX = posX - width/2 + paddingX;

			//Iterate through characters
			for (int i = 0; i < text.size(); i++)
			{
				//Get width of the current character
				int charWidth = font->getTextWidth(text.substr(i, 1));

				//Move cursor before or after character depending on mouse position
				if (mouseX >= textX && mouseX <= textX + charWidth/2)
				{
					cursorPos = i;
					adjustCamera(TBOX_LEFT_RIGHT);
					break;
				}
				else if (mouseX >= textX + charWidth/2 && mouseX < textX + charWidth)
				{
					cursorPos = i+1;
					adjustCamera(TBOX_LEFT_RIGHT);
					break;
				}
				else if (mouseX >= textX + charWidth && i == text.size()-1)
				{
					//Move cursor to end of string if mouse position is beyond end of string
					cursorPos = text.size();
					break;
				}

				textX += charWidth;

				//Move to newline
				if (textX > posX + width/2)
					break;
			}
		}

		if (highlighted(mouseX, mouseY))
		{
			active = true;
		}
		else
		{
			active = false;
		}
	}
}

void Textbox::processCharacterCallback(unsigned int codepoint)
{
	if (visible && active)
	{
		if (textLimit == 0 || text.size() < textLimit)
		{
			text.insert(cursorPos++, 1, codepoint);
			adjustCamera(TBOX_ADD);
		}
	}
}

//================================================================================================
//Miscellaneous
//================================================================================================

void Textbox::update(double deltaTime)
{
	if (active)
	{
		//Decrease cursor time
		cursorTime -= deltaTime;

		//Reset cursor time
		if (cursorTime <= 0)
			cursorTime += 1;
	}
}

void Textbox::render(int windowWidth, int windowHeight)
{
	if (visible)
	{
		//Matrices
		glm::mat4 projection(glm::ortho(0.0f, (float) windowWidth, 0.0f, (float) windowHeight));
		glm::mat4 view(1.0f);
		glm::mat4 cameraView = glm::lookAt(glm::vec3((float) cameraPos, 0.0f, 0.0f),
				glm::vec3((float) cameraPos, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(posX, posY, 0.0f));
		model = glm::scale(model, glm::vec3(width, height, 1.0f));

		//Render background and border
		background.render(projection, view, model);
		border.renderLineLoop(projection, view, model);

		//Set up text viewport
		int viewportX = posX - width/2;
		int viewportY = posY - height/2;
		int viewportWidth = width - 1;
		int viewportHeight = height - 1;
		glViewport(viewportX, viewportY, viewportWidth, viewportHeight);

		//Viewport appropriate projection matrix
		projection = glm::ortho((float) viewportX, (float) viewportX + viewportWidth,
				(float) viewportY, (float) viewportY + viewportHeight);

		//Text rendering variables
		int textX = posX - width/2 + paddingX;
		int textY = posY - height/4;
		int cursorX = 0;
		int cursorY = 0;
		int textWidth = 0;

		//Iterate through characters to get cursor position
		for (int i = 0; active && i <= text.size(); i++)
		{
			if (i == cursorPos)
			{
				//Set cursor position
				cursorX = textX + textWidth - font->getTextWidth("|")/2 + 1;
				cursorY = textY;
			}

			if (i < text.size())
			{
				//Get width of the current character
				textWidth += font->getTextWidth(text.substr(i, 1));
			}
		}

		//Render text
		font->renderText(text, projection, cameraView, textX, textY, 1.0f,
				textColour, LEFT_ALIGN);

		//Render cursor
		if (active && cursorTime >= 0.5)
			font->renderText("|", projection, cameraView, cursorX, cursorY, 1.0f,
					textColour, LEFT_ALIGN);

		//Reset viewport
		glViewport(0, 0, windowWidth, windowHeight);
	}
}

void Textbox::adjustCamera(int action)
{
	int textX = paddingX;

	//Get text position at new cursor position
	for (int i = 0; i < cursorPos; i++)
		textX += font->getTextWidth(text.substr(i, 1));

	//Get total text width
	int textWidth = font->getTextWidth(text) + paddingX;

	//Adjust camera position
	switch (action)
	{
		case TBOX_LEFT_RIGHT:
		case TBOX_ADD:
		case TBOX_PASTE:
			if (cursorPos == 0 || textWidth < width)
				cameraPos = 0;
			else if (textX <= cameraPos)
				cameraPos = textX;
			else if (textX > cameraPos + width)
				cameraPos = textX - width + paddingX*2;
			break;
		case TBOX_HOME:
			cameraPos = 0;
			break;
		case TBOX_END:
			cameraPos = textWidth - width + paddingX*2;
			break;
		case TBOX_DELETE:
			if (cursorPos == 0 || textWidth < width)
				cameraPos = 0;
			else if (textWidth >= width && textWidth <= cameraPos + width)
				cameraPos = textWidth - width + paddingX*2;
			break;
	}
}

//================================================================================================
//Getters
//================================================================================================

std::string Textbox::getText()
{
	return text;
}

bool Textbox::getActive()
{
	return active;
}

//================================================================================================
//Setters
//================================================================================================

void Textbox::setActive(bool newActive)
{
	active = newActive;
}

void Textbox::setText(std::string newText)
{
	text = newText;
}

void Textbox::setTextLimit(int newLimit)
{
	textLimit = newLimit;
}

void Textbox::setTextColour(glm::vec4 colour)
{
	textColour = colour;
}

void Textbox::setBorderColour(glm::vec4 colour)
{
	border.setColour(colour);
}

void Textbox::setBackgroundColour(glm::vec4 colour)
{
	background.setColour(colour);
}

void Textbox::setPadding(int x)
{
	paddingX = x;
}
