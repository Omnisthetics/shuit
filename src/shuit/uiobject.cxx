#include "./uiobject.h"
#include <GLFW/glfw3.h>

//================================================================================================
//Constructor
//================================================================================================

UiObject::UiObject(GLFWwindow * newWindow, int newHoriMode, int newVertMode, float newX, float newY,
		float newWidth, float newHeight)
{
	window = newWindow;

	horiPosMode = newHoriMode;
	vertPosMode = newVertMode;

	modX = newX;
	modY = newY;

	width = newWidth;
	height = newHeight;

	visible = true;

	reposition();
}

UiObject::UiObject(const UiObject & uiObject)
{
	window = uiObject.window;
	horiPosMode = uiObject.horiPosMode;
	vertPosMode = uiObject.vertPosMode;
	modX = uiObject.modX;
	modY = uiObject.modY;
	posX = uiObject.posX;
	posY = uiObject.posY;
	width = uiObject.width;
	height = uiObject.height;
	visible = uiObject.visible;
}

UiObject & UiObject::operator=(const UiObject & uiObject)
{
	window = uiObject.window;
	horiPosMode = uiObject.horiPosMode;
	vertPosMode = uiObject.vertPosMode;
	modX = uiObject.modX;
	modY = uiObject.modY;
	posX = uiObject.posX;
	posY = uiObject.posY;
	width = uiObject.width;
	height = uiObject.height;
	visible = uiObject.visible;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

bool UiObject::highlighted(double mouseX, double mouseY)
{
	if (visible && mouseX >= posX-width/2 && mouseX <= posX+width/2
			&& mouseY >= posY-height/2 && mouseY <= posY+height/2)
		return true;
	else
		return false;
}


void UiObject::reposition()
{
	int windowWidth;
	int windowHeight;
	glfwGetWindowSize(window, &windowWidth, &windowHeight);

	switch (horiPosMode)
	{
		case STATIC:
		case LEFT_OFFSET:
			posX = modX;
			break;
		case LEFT_MARGIN:
			posX = modX + width/2;
			break;
		case RIGHT_OFFSET:
			posX = windowWidth - modX;
			break;
		case RIGHT_MARGIN:
			posX = windowWidth - modX - width/2;
			break;
		case MIDDLE_OFFSET:
			posX = windowWidth/2 + modX;
			break;
		case MIDDLE_MARGIN:
			if (modX < 0)
				posX = windowWidth/2 + modX - width/2;
			else if (modX > 0)
				posX = windowWidth/2 + modX + width/2;
			else
				posX = windowWidth/2;
			break;
		case PERCENTAGE:
			posX = (float) windowWidth * modX;
			break;
	}

	switch (vertPosMode)
	{
		case STATIC:
		case DOWN_OFFSET:
			posY = modY;
			break;
		case DOWN_MARGIN:
			posY = modY + height/2;
			break;
		case UP_OFFSET:
			posY = windowHeight - modY;
			break;
		case UP_MARGIN:
			posY = windowHeight - modY - height/2;
			break;
		case MIDDLE_OFFSET:
			posY = windowHeight/2 + modY;
			break;
		case MIDDLE_MARGIN:
			if (modY < 0)
				posY = windowHeight/2 + modY - height/2;
			else if (modY > 0)
				posY = windowHeight/2 + modY + height/2;
			else
				posY = windowHeight/2;
			break;
		case PERCENTAGE:
			posY = (float) windowHeight * modY;
			break;
	}
}

//================================================================================================
//Getters
//================================================================================================

int UiObject::getHoriPosMode()
{
	return horiPosMode;
}

int UiObject::getVertPosMode()
{
	return vertPosMode;
}

bool UiObject::getVisible()
{
	return visible;
}

float UiObject::getPosX()
{
	return posX;
}

float UiObject::getPosY()
{
	return posY;
}

float UiObject::getModX()
{
	return modX;
}

float UiObject::getModY()
{
	return modY;
}

float UiObject::getWidth()
{
	return width;
}

float UiObject::getHeight()
{
	return height;
}

//================================================================================================
//Setters
//================================================================================================

void UiObject::hide()
{
	visible = false;
}

void UiObject::reveal()
{
	visible = true;
}

void UiObject::setModX(int newHoriPosMode, float newX)
{
	horiPosMode = newHoriPosMode;
	modX = newX;

	reposition();
}

void UiObject::setModY(int newVertPosMode, float newY)
{
	vertPosMode = newVertPosMode;
	modY = newY;

	reposition();
}

void UiObject::setWidth(float newWidth)
{
	width = newWidth;

	reposition();
}

void UiObject::setHeight(float newHeight)
{
	height = newHeight;

	reposition();
}
