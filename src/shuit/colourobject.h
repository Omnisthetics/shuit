#ifndef COLOUROBJECT_H_
#define COLOUROBJECT_H_

#include "./shader.h"

//TODO: Consider making this a UI object

class ColourObject
{
private:
	GLuint VAO;
	GLuint VBO;
	GLuint EBO;
	Shader shader;
	glm::vec4 colour;
public:
	//Constructors
	ColourObject() {};
	ColourObject(glm::vec4 newColour);
	ColourObject(const ColourObject & other);
	ColourObject & operator=(const ColourObject & other);

	//Misc.
	void render(glm::mat4 projection, glm::mat4 view, glm::mat4 model);
	void renderLineLoop(glm::mat4 projection, glm::mat4 view, glm::mat4 model);

	//Getters
	glm::vec4 getColour();

	//Setters
	void setColour(glm::vec4 newColour);
};

#endif
