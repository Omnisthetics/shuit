#include "./texture2d.h"
#include "./stb_image.h"
#include <iostream>

//================================================================================================
//Constructors
//================================================================================================

Texture2D::Texture2D(const std::string & texFile)
{
	glGenTextures(1, &ID);
	glBindTexture(GL_TEXTURE_2D, ID);

	//Disable byte-alignment restriction
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned char * data = stbi_load(texFile.c_str(), &width, &height, &channels, 0);
	if (data != nullptr)
	{
		if (channels == 1)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
		else if (channels == 3)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		else if (channels == 4)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture!" << std::endl;
	}

	stbi_image_free(data);
}

Texture2D::Texture2D(const Texture2D & texture)
{
	ID = texture.ID;
	width = texture.width;
	height = texture.height;
	channels = texture.channels;
}

Texture2D & Texture2D::operator=(const Texture2D & texture)
{
	ID = texture.ID;
	width = texture.width;
	height = texture.height;
	channels = texture.channels;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

void Texture2D::bind()
{
	glBindTexture(GL_TEXTURE_2D, ID);
}

void Texture2D::setParameteri(GLenum name, GLenum param)
{
	//Get current bound texture
	GLint oldBind;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &oldBind);

	//Bind this texture and adjust parameter
	glBindTexture(GL_TEXTURE_2D, ID);
	glTexParameteri(GL_TEXTURE_2D, name, param);

	//Rebind old texture
	glBindTexture(GL_TEXTURE_2D, oldBind);
}

void Texture2D::setParameterf(GLenum name, GLfloat param)
{
	//Get current bound texture
	GLint oldBind;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &oldBind);

	//Bind this texture and adjust parameter
	glBindTexture(GL_TEXTURE_2D, ID);
	glTexParameterf(GL_TEXTURE_2D, name, param);

	//Rebind old texture
	glBindTexture(GL_TEXTURE_2D, oldBind);
}

//================================================================================================
//Getters
//================================================================================================

GLuint Texture2D::getID()
{
	return ID;
}

int Texture2D::getWidth()
{
	return width;
}

int Texture2D::getHeight()
{
	return height;
}

int Texture2D::getChannels()
{
	return channels;
}
