#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include <GL/gl.h>
#include <string>

class Texture2D
{
private:
	GLuint ID;
	int width;
	int height;
	int channels;
public:
	//Constructors
	Texture2D() {};
	Texture2D(const std::string & texFile);
	Texture2D(const Texture2D & texture);
	Texture2D & operator=(const Texture2D & texture);

	//Misc.
	void bind();
	void setParameteri(GLenum name, GLenum param);
	void setParameterf(GLenum name, GLfloat param);

	//Getters
	GLuint getID();
	int getWidth();
	int getHeight();
	int getChannels();
};

#endif
