#ifndef TEXTBOX_H_
#define TEXTBOX_H_

#include "./colourobject.h"
#include "./uiobject.h"

class Font;

//TODO: Consider adding a y padding variable
//TODO: Highlighting
//TODO: Copy

enum Actions
{
	TBOX_LEFT_RIGHT,
	TBOX_HOME,
	TBOX_END,
	TBOX_ADD,
	TBOX_DELETE,
	TBOX_PASTE
};

class Textbox : public UiObject
{
private:
	Font * font;
	bool active;

	//Text members
	std::string text;
	glm::vec4 textColour;
	int textLimit;
	int cursorPos;
	int cameraPos;
	int paddingX;
	double cursorTime;

	//Non-text colours
	ColourObject border;
	ColourObject background;
public:
	//Constructors
	Textbox() {};
	Textbox(Font * newFont, GLFWwindow * window, int newHoriMode, int newVertMode,
			float newX, float newY, float newW, float newH);
	Textbox(const Textbox & other);
	Textbox & operator=(const Textbox & other);

	//Input management
	void processKeyboardCallback(int key, int scancode, int action, int mods);
	void processMouseCallback(int button, int action, int mods);
	void processCharacterCallback(unsigned int codepoint);

	//Misc.
	void update(double deltaTime);
	void render(int windowWidth, int windowHeight);
	void adjustCamera(int action);

	//Getters
	std::string getText();
	bool getActive();

	//Setters
	void setActive(bool newActive);
	void setText(std::string newText);
	void setTextLimit(int newLimit);
	void setTextColour(glm::vec4 colour);
	void setBorderColour(glm::vec4 colour);
	void setBackgroundColour(glm::vec4 colour);
	void setPadding(int x);
};

#endif
