#include "./multilinetextbox.h"
#include "./font.h"
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

//================================================================================================
//Constructor
//================================================================================================

MultilineTextbox::MultilineTextbox(Font * newFont, GLFWwindow * window, int newHoriMode,
		int newVertMode, float newX, float newY, float newW, float newH)
		: UiObject(window, newHoriMode, newVertMode, newX, newY, newW, newH)
{
	text = "";
	font = newFont;

	active = false;

	cursorPos = 0;
	textLimit = 0;
	lineHeight = font->getTextHeight("|");
	paddingX = 0;
	paddingY = 0;
	cursorTime = 1;

	textColour = glm::vec4(1.0f);
	border = ColourObject(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));
	background = ColourObject(glm::vec4(0.0f, 0.0f, 0.0f, 0.0f));
}

MultilineTextbox::MultilineTextbox(const MultilineTextbox & other) : UiObject(other)
{
	font = other.font;
	active = other.active;

	text = other.text;
	textColour = other.textColour;
	textLimit = other.textLimit;
	lineHeight = other.lineHeight;
	cursorPos = other.cursorPos;
	paddingX = other.paddingX;
	paddingY = other.paddingY;
	cursorTime = other.cursorTime;

	border = other.border;
	background = other.background;
}

MultilineTextbox & MultilineTextbox::operator=(const MultilineTextbox & other)
{
	window = other.window;
	horiPosMode = other.horiPosMode;
	vertPosMode = other.vertPosMode;
	modX = other.modX;
	modY = other.modY;
	posX = other.posX;
	posY = other.posY;
	width = other.width;
	height = other.height;
	visible = other.visible;

	font = other.font;
	active = other.active;

	text = other.text;
	textColour = other.textColour;
	textLimit = other.textLimit;
	lineHeight = other.lineHeight;
	cursorPos = other.cursorPos;
	paddingX = other.paddingX;
	paddingY = other.paddingY;
	cursorTime = other.cursorTime;

	border = other.border;
	background = other.background;

	return *this;
}

//================================================================================================
//Input Management
//================================================================================================

void MultilineTextbox::processKeyboardCallback(int key, int scancode, int action, int mods)
{
	int cursorX;
	int cursorY;

	if (visible && active)
	{
		if (action == GLFW_PRESS || action == GLFW_REPEAT)
		{
			switch (key)
			{
				case GLFW_KEY_LEFT:
					if (cursorPos > 0)
						cursorPos--;
					break;

				case GLFW_KEY_RIGHT:
					if (cursorPos < text.size())
						cursorPos++;
					break;

				case GLFW_KEY_HOME:
					cursorPos = 0;
					break;

				case GLFW_KEY_END:
					cursorPos = text.size();
					break;

				case GLFW_KEY_BACKSPACE:
					if (cursorPos > 0)
					{
						text.erase(cursorPos-1, 1);
						cursorPos--;
					}
					break;

				case GLFW_KEY_DELETE:
					if (cursorPos < text.size())
					{
						text.erase(cursorPos, 1);
					}
					break;

				case GLFW_KEY_V:
					if (mods == GLFW_MOD_CONTROL)
					{
						std::string clipboardText = glfwGetClipboardString(window);
						text.insert(cursorPos, clipboardText);
						cursorPos += clipboardText.size();
					}
					break;

				case GLFW_KEY_UP:
					calcCursorCoords(cursorX, cursorY);
					std::cout << cursorX << ", " << cursorY << std::endl;
					cursorPos = calcCursorPos(cursorX, cursorY + lineHeight);
					break;

				case GLFW_KEY_DOWN:
					calcCursorCoords(cursorX, cursorY);
					std::cout << cursorX << ", " << cursorY << std::endl;
					cursorPos = calcCursorPos(cursorX, cursorY - lineHeight);
					break;
			}
		}
	}
}

void MultilineTextbox::processMouseCallback(int button, int action, int mods)
{
	double mouseX;
	double mouseY;
	glfwGetCursorPos(window, &mouseX, &mouseY);

	int windowWidth;
	int windowHeight;
	glfwGetWindowSize(window, &windowWidth, &windowHeight);

	//Convert mouseY to standard coordinates
	mouseY = windowHeight - mouseY;

	if (visible && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		if (active && highlighted(mouseX, mouseY))
		{
			int textX = posX - width/2 + paddingX;
			int textY = posY + height/2 - lineHeight - paddingY;

			//Iterate through characters
			for (int i = 0; i < text.size(); i++)
			{
				//Get width of the current character
				int charWidth = font->getTextWidth(text.substr(i, 1));

				if (textY <= mouseY)
				{
					//Move cursor before or after character depending on mouse position
					if (mouseX >= textX && mouseX <= textX + charWidth/2)
					{
						cursorPos = i;
						break;
					}
					else if (mouseX >= textX + charWidth/2 && mouseX < textX + charWidth)
					{
						cursorPos = i+1;
						break;
					}
				}

				textX += charWidth;

				//Move to newline
				if (textX > posX + width/2)
				{
					//Repeat character on newline
					textX = posX - width/2 + paddingX;
					textY -= font->getTextHeight("|");
					i--;
				}
			}
		}

		if (highlighted(mouseX, mouseY))
		{
			active = true;
		}
		else
		{
			active = false;
		}
	}
}

void MultilineTextbox::processCharacterCallback(unsigned int codepoint)
{
	if (visible && active)
	{
		if (textLimit == 0 || text.size() < textLimit)
		{
			text.insert(cursorPos++, 1, codepoint);
		}
	}
}

//================================================================================================
//Miscellaneous
//================================================================================================

void MultilineTextbox::update(double deltaTime)
{
	if (active)
	{
		//Decrease cursor time
		cursorTime -= deltaTime;

		//Reset cursor time
		if (cursorTime <= 0)
			cursorTime += 1;
	}
}

void MultilineTextbox::render(int windowWidth, int windowHeight)
{
	if (visible)
	{
		//Matrices
		glm::mat4 projection(glm::ortho(0.0f, (float) windowWidth, 0.0f, (float) windowHeight));
		glm::mat4 view(1.0f);
		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(posX, posY, 0.0f));
		model = glm::scale(model, glm::vec3(width, height, 1.0f));

		//Render background and border
		background.render(projection, view, model);
		border.renderLineLoop(projection, view, model);

		//Set up text viewport
		int viewportX = posX - width/2 - 1;
		int viewportY = posY - height/2 - 1;
		int viewportWidth = width + 1;
		int viewportHeight = height + 1;
		glViewport(viewportX, viewportY, viewportWidth, viewportHeight);

		//Viewport appropriate projection matrix
		projection = glm::ortho((float) viewportX, (float) viewportX + viewportWidth,
				(float) viewportY, (float) viewportY + viewportHeight);

		//Text rendering variables
		int textX = posX - width/2 + paddingX;
		int textY = posY + height/2 - lineHeight - paddingY;
		int cursorX = 0;
		int cursorY = 0;
		int line = 0;
		int textWidth = 0;
		int lineStart = 0;

		//Iterate through characters
		for (int i = 0; i <= text.size(); i++)
		{
			if (i == cursorPos)
			{
				//Set cursor position
				cursorX = textX + textWidth - font->getTextWidth("|")/2 + 1;
				cursorY = textY - line * lineHeight;
			}

			if (i < text.size())
			{
				//Get width of the current character
				textWidth += font->getTextWidth(text.substr(i, 1));

				if (textWidth + paddingX > width)
				{
					//Render substring
					font->renderText(text.substr(lineStart, i-lineStart), projection, view, textX,
							textY-line*lineHeight, 1.0f, textColour, LEFT_ALIGN);

					line++;
					lineStart = i;
					textWidth = font->getTextWidth(text.substr(i, 1));
				}
			}
		}

		//Render remaining text
		font->renderText(text.substr(lineStart), projection, view, textX,
				textY-line*lineHeight, 1.0f, textColour, LEFT_ALIGN);

		//Render cursor
		if (active && cursorTime >= 0.5)
			font->renderText("|", projection, view, cursorX, cursorY, 1.0f,
					textColour, LEFT_ALIGN);

		//Reset viewport
		glViewport(0, 0, windowWidth, windowHeight);
	}
}

//================================================================================================
//Getters
//================================================================================================

std::string MultilineTextbox::getText()
{
	return text;
}

bool MultilineTextbox::getActive()
{
	return active;
}

void MultilineTextbox::calcCursorCoords(int & x, int & y)
{
	int textX = posX - width/2 + paddingX;
	int textY = posY + height/2 - lineHeight - paddingY;
	int line = 0;
	int textWidth = 0;
	int lineStart = 0;

	//Iterate through characters
	for (int i = 0; i <= text.size(); i++)
	{
		if (i == cursorPos)
		{
			//Set cursor position
			x = textX + textWidth - font->getTextWidth("|")/2 + 1;
			y = textY - line * lineHeight;
			break;
		}

		if (i < text.size())
		{
			//Get width of the current character
			textWidth += font->getTextWidth(text.substr(i, 1));

			if (textWidth + paddingX > width)
			{
				line++;
				lineStart = i;
				textWidth = font->getTextWidth(text.substr(i, 1));
			}
		}
	}
}

int MultilineTextbox::calcCursorPos(int x, int y)
{
	int textX = posX - width/2 + paddingX;
	int textY = posY + height/2 - lineHeight - paddingY;
	int line = 0;
	int textWidth = 0;

	//Iterate through characters
	for (int i = 0; i < text.size(); i++)
	{
		//Get width of the current character
		textWidth += font->getTextWidth(text.substr(i, 1));

		//Move down a line
		if (textWidth + paddingX > width)
		{
			line++;
			textWidth = font->getTextWidth(text.substr(i, 1));
		}

		//Return current character index as position if coordinates passed
		if (textX + textWidth >= x && textY + (line * lineHeight) >= y)
			return i;
	}

	return text.size();
}

//================================================================================================
//Setters
//================================================================================================

void MultilineTextbox::setActive(bool newActive)
{
	active = newActive;
}

void MultilineTextbox::setText(std::string newText)
{
	text = newText;
}

void MultilineTextbox::setTextLimit(int newLimit)
{
	textLimit = newLimit;
}

void MultilineTextbox::setTextColour(glm::vec4 colour)
{
	textColour = colour;
}

void MultilineTextbox::setBorderColour(glm::vec4 colour)
{
	border.setColour(colour);
}

void MultilineTextbox::setBackgroundColour(glm::vec4 colour)
{
	background.setColour(colour);
}

void MultilineTextbox::setPadding(int x, int y)
{
	paddingX = x;
	paddingY = y;
}
