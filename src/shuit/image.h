#ifndef IMAGE_H_
#define IMAGE_H_

#include "./texture2d.h"
#include "./shader.h"

struct Rect
{
	float x;
	float y;
	float w;
	float h;
};

class Image
{
private:
	Texture2D texture;
	std::string filepath;
	GLuint VAO;
	GLuint VBO;
	GLuint EBO;
	Shader * shader;
	GLint glTexMinFilter;
	GLint glTexMagFilter;
public:
	//Constructors
	Image() {};
	Image(std::string newPath, GLint newMinFilter, GLint newMagFilter, Shader * newShader);
	Image(const Image & image);
	Image & operator=(const Image & image);
	//~Image();

	//Misc.
	void loadImage(std::string newPath, GLint glTexMinFilter, GLint glTexMagFilter);
	void render(glm::mat4 projection, glm::mat4 view, glm::mat4 model, float width, float height);
	void renderClip(glm::mat4 projection, glm::mat4 view, glm::mat4 model, float width,
			float height, Rect clip);

	//Getters
	int getWidth();
	int getHeight();
	Texture2D * getTexture();
	std::string getFilepath();
};

#endif
