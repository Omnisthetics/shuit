#include "./button.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

//================================================================================================
//Constructor
//================================================================================================

Button::Button(std::string texName, GLFWwindow * window, Shader * imageShader, int newHoriMode,
		int newVertMode, float newX, float newY, float newWidth, float newHeight)
		: UiObject(window, newHoriMode, newVertMode, newX, newY, newWidth, newHeight)
{
	image = Image(texName, GL_LINEAR, GL_LINEAR, imageShader);
	imageClip = {NULL, NULL, NULL, NULL};

	border = ColourObject(glm::vec4(0.0f));
	background = ColourObject(glm::vec4(0.0f));
}

Button::Button(const Button & other) : UiObject(other)
{
	image = other.image;
	imageClip = other.imageClip;
	border = other.border;
	background = other.background;
}

Button & Button::operator=(const Button & other)
{
	//UiObject members
	window = other.window;
	horiPosMode = other.horiPosMode;
	vertPosMode = other.vertPosMode;
	modX = other.modX;
	modY = other.modY;
	posX = other.posX;
	posY = other.posY;
	width = other.width;
	height = other.height;
	visible = other.visible;

	//Button members
	image = other.image;
	imageClip = other.imageClip;
	border = other.border;
	background = other.background;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

void Button::render(glm::mat4 projection, glm::mat4 view)
{
	if (visible)
	{
		//Model matrix
		glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(posX, posY, 0));

		//Render background and border
		background.render(projection, view, model);
		border.render(projection, view, model);

		//Render image
		if (imageClip.w == NULL || imageClip.h == NULL)
			image.render(projection, view, model, width, height);
		else
			image.renderClip(projection, view, model, width, height, imageClip);
	}
}

//================================================================================================
//Getters
//================================================================================================

Image * Button::getImage()
{
	return &image;
}

//================================================================================================
//Getters
//================================================================================================

void Button::setImage(std::string imageName)
{
	image.loadImage(imageName, GL_LINEAR, GL_LINEAR);
}

void Button::setImageClip(Rect newClip)
{
	imageClip = newClip;
}

void Button::setBorderColour(glm::vec4 colour)
{
	border.setColour(colour);
}

void Button::setBackgroundColour(glm::vec4 colour)
{
	background.setColour(colour);
}
