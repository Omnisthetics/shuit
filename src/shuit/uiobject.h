#ifndef UIOBJECT_H_
#define UIOBJECT_H_

struct GLFWwindow;

//TODO: Consider making this a virtual class

enum PositionModes
{
	LEFT_OFFSET,
	LEFT_MARGIN,
	RIGHT_OFFSET,
	RIGHT_MARGIN,
	DOWN_OFFSET,
	DOWN_MARGIN,
	UP_OFFSET,
	UP_MARGIN,
	MIDDLE_OFFSET,
	MIDDLE_MARGIN,
	PERCENTAGE,
	STATIC
};

class UiObject
{
protected:
	GLFWwindow * window;
	int horiPosMode;
	int vertPosMode;
	float modX;
	float modY;
	float posX;
	float posY;
	float width;
	float height;
	bool visible;
public:
	//Constructors
	UiObject() {};
	UiObject(GLFWwindow * newWindow, int newHoriMode, int newVertMode, float newX, float newY,
			float newWidth, float newHeight);
	UiObject(const UiObject & uiObject);
	UiObject & operator=(const UiObject & uiObject);

	//Misc.
	bool highlighted(double mouseX, double mouseY);
	void reposition();

	//Getters
	int getHoriPosMode();
	int getVertPosMode();
	bool getVisible();
	float getPosX();
	float getPosY();
	float getModX();
	float getModY();
	float getWidth();
	float getHeight();

	//Setters
	void hide();
	void reveal();
	void setModX(int newHoriPosMode, float newX);
	void setModY(int newVertPosMode, float newY);
	void setWidth(float newWidth);
	void setHeight(float newHeight);
};

#endif
