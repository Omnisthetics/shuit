#ifndef MULTILINETEXTBOX_H_
#define MULTILINETEXTBOX_H_

#include "./colourobject.h"
#include "./uiobject.h"
#include <string>
#include <glm/glm.hpp>

//TODO: Highlighting
//TODO:	Cursor focus/"Camera" movement
//TODO: Copy
//TODO: Up/Down

class Font;
class ColourObject;

class MultilineTextbox : public UiObject
{
private:
	Font * font;
	bool active;

	//Text members
	std::string text;
	glm::vec4 textColour;
	int textLimit;
	int cursorPos;
	int lineHeight;
	int paddingX;
	int paddingY;
	double cursorTime;

	//Non-text colours
	ColourObject border;
	ColourObject background;
public:
	//Constructors
	MultilineTextbox() {};
	MultilineTextbox(Font * newFont, GLFWwindow * window, int newHoriMode, int newVertMode,
			float newX, float newY, float newW, float newH);
	MultilineTextbox(const MultilineTextbox & other);
	MultilineTextbox & operator=(const MultilineTextbox & other);

	//Input management
	void processKeyboardCallback(int key, int scancode, int action, int mods);
	void processMouseCallback(int button, int action, int mods);
	void processCharacterCallback(unsigned int codepoint);

	//Misc.
	void update(double deltaTime);
	void render(int windowWidth, int windowHeight);
	void calcCursorCoords(int & x, int & y);
	int calcCursorPos(int x, int y);

	//Getters
	std::string getText();
	bool getActive();

	//Setters
	void setActive(bool newActive);
	void setText(std::string newText);
	void setTextLimit(int newLimit);
	void setTextColour(glm::vec4 colour);
	void setBorderColour(glm::vec4 colour);
	void setBackgroundColour(glm::vec4 colour);
	void setPadding(int x, int y);
};

#endif
