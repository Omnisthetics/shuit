#include "./textbutton.h"
#include "./font.h"
#include <glm/gtc/matrix_transform.hpp>

//================================================================================================
//Constructor
//================================================================================================

TextButton::TextButton(std::string newText, Font * newFont, GLFWwindow * newWindow,
		int newHoriMode, int newVertMode, float newX, float newY)
		: UiObject(newWindow, newHoriMode, newVertMode, newX, newY, 0, 0)
{
	//Forced size members
	forcedWidth = NULL;
	forcedHeight = NULL;

	//Extra size members
	extraWidth = 0;
	extraHeight = 0;

	//Text members
	text = newText;
	font = newFont;
	textAlignment = CENTER_ALIGN;
	textColour = glm::vec4(1.0f);

	width = font->getTextWidth(text);
	height = font->getTextHeight(text);
	reposition();

	//Padding members
	paddingX = 0;

	//Non-text colours
	border = ColourObject(glm::vec4(1.0f));
	background = ColourObject(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	//Icon members
	iconVisible = false;
	iconWidth = 0;
	iconHeight = 0;
	iconTextGap = 0;
}

TextButton::TextButton(const TextButton & other) : UiObject(other)
{
	font = other.font;

	forcedWidth = other.forcedWidth;
	forcedHeight = other.forcedHeight;
	extraWidth = other.extraWidth;
	extraHeight = other.extraHeight;

	text = other.text;
	textColour = other.textColour;
	textAlignment = other.textAlignment;

	iconVisible = other.iconVisible;
	icon = other.icon;
	iconWidth = other.iconWidth;
	iconHeight = other.iconHeight;
	iconTextGap = other.iconTextGap;

	paddingX = other.paddingX;

	border = other.border;
	background = other.background;
}

TextButton & TextButton::operator=(const TextButton & other)
{
	window = other.window;
	horiPosMode = other.horiPosMode;
	vertPosMode = other.vertPosMode;
	modX = other.modX;
	modY = other.modY;
	posX = other.posX;
	posY = other.posY;
	width = other.width;
	height = other.height;
	visible = other.visible;

	font = other.font;

	forcedWidth = other.forcedWidth;
	forcedHeight = other.forcedHeight;
	extraWidth = other.extraWidth;
	extraHeight = other.extraHeight;

	text = other.text;
	textColour = other.textColour;
	textAlignment = other.textAlignment;

	iconVisible = other.iconVisible;
	icon = other.icon;
	iconWidth = other.iconWidth;
	iconHeight = other.iconHeight;
	iconTextGap = other.iconTextGap;

	paddingX = other.paddingX;

	border = other.border;
	background = other.background;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

void TextButton::render(glm::mat4 projection, glm::mat4 view)
{
	if (visible)
	{
		//Model matrix
		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(posX, posY, 0.0f));
		model = glm::scale(model, glm::vec3(width, height, 0.0f));

		//Render background and border
		background.render(projection, view, model);
		border.renderLineLoop(projection, view, model);

		//Determine text position
		int textX;
		int textY;
		switch (textAlignment)
		{
			case LEFT_ALIGN:
				textX = posX - width/2 + paddingX;
				textY = posY - font->getTextHeight(text)/2;
				break;
			case RIGHT_ALIGN:
				textX = posX + width/2 - paddingX;
				textY = posY - font->getTextHeight(text)/2;
				break;
			case CENTER_ALIGN:
				textX = posX;
				textY = posY - font->getTextHeight(text)/2;
				break;
		}

		//Render icon
		if (iconVisible)
		{
			int iconX;
			int iconY;

			switch (textAlignment)
			{
				case LEFT_ALIGN:
					iconX = textX + iconWidth/2;
					iconY = posY;
					textX += iconWidth + iconTextGap;
					break;
				case RIGHT_ALIGN:
					iconX = textX - iconWidth/2;
					iconY = posY;
					textX -= iconWidth + iconTextGap;
					break;
				case CENTER_ALIGN:
					float contentWidth = iconWidth + iconTextGap + font->getTextWidth(text);
					iconX = posX - contentWidth/2 + iconWidth/2;
					iconY = posY;
					textX = posX - contentWidth/2 + iconWidth + iconTextGap + font->getTextWidth(text)/2;
					break;
			}

			model = glm::translate(glm::mat4(1.0f), glm::vec3(iconX, iconY, 0.0f));
			icon.render(projection, view, model, iconWidth, iconHeight);
		}

		//Determine whether to trim text to fit button
		int contentWidth = font->getTextWidth(text) + paddingX + iconWidth + iconTextGap;
		if (contentWidth > width)
		{
			std::string trimmedText = text;

			contentWidth = font->getTextWidth(trimmedText + "...") + paddingX
					+ iconWidth + iconTextGap;
			while (contentWidth > width)
			{
				//Trim last character
				trimmedText.pop_back();
				contentWidth = font->getTextWidth(trimmedText + "...") + paddingX
						+ iconWidth + iconTextGap;
			}

			//Render trimmed text
			font->renderText(trimmedText + "...", projection, view, textX, textY, 1.0f,
					textColour, textAlignment);
		}
		else
		{
			//Render standard text
			font->renderText(text, projection, view, textX, textY, 1.0f,
					textColour, textAlignment);
		}
	}
}

void TextButton::resize()
{
	//Width resize
	if (forcedWidth != NULL)
	{
		width = forcedWidth;
	}
	else
	{
		width = font->getTextWidth(text) + paddingX + extraWidth;

		if (iconVisible)
		{
			width += iconWidth + iconTextGap;
		}
	}

	//Height resize
	if (forcedHeight != NULL)
	{
		height = forcedHeight;
	}
	else
	{
		height = font->getTextHeight(text);

		if (iconVisible)
		{
			if (iconHeight > height)
				height = iconHeight + extraHeight;
			else
				height += extraHeight;
		}
		else
		{
			height = font->getTextHeight(text) + extraHeight;
		}
	}
}

//================================================================================================
//Getters
//================================================================================================

std::string TextButton::getText()
{
	return text;
}

int TextButton::getContentWidth()
{
	return paddingX + iconWidth + iconTextGap + font->getTextWidth(text);
}

//================================================================================================
//Setters
//================================================================================================

void TextButton::setForcedWidth(int w)
{
	forcedWidth = w;
	resize();
}

void TextButton::setForcedHeight(int h)
{
	forcedHeight = h;
	resize();
}

void TextButton::setExtraWidth(int extra)
{
	extraWidth = extra;
	resize();
}

void TextButton::setExtraHeight(int extra)
{
	extraHeight = extra;
	resize();
}

void TextButton::setText(std::string newText)
{
	text = newText;
	resize();
}

void TextButton::setTextColour(glm::vec4 colour)
{
	textColour = colour;
}

void TextButton::setTextAlignment(int alignment)
{
	textAlignment = alignment;
}

void TextButton::setBorderColour(glm::vec4 colour)
{
	border.setColour(colour);
}

void TextButton::setBackgroundColour(glm::vec4 colour)
{
	background.setColour(colour);
}

void TextButton::setPadding(int x)
{
	paddingX = x;
}

void TextButton::setIcon(std::string iconFile, Shader * iconShader)
{
	icon = Image(iconFile, GL_LINEAR, GL_LINEAR, iconShader);
	iconVisible = true;
	resize();
}

void TextButton::setIcon(Image newIcon)
{
	icon = newIcon;
	iconVisible = true;
	resize();
}

void TextButton::setIconWidth(int width)
{
	iconWidth = width;
	resize();
}

void TextButton::setIconHeight(int height)
{
	iconHeight = height;
	resize();
}

void TextButton::setIconTextGap(int gap)
{
	iconTextGap = gap;
}

void TextButton::setIconVisibility(bool vis)
{
	iconVisible = vis;
}
