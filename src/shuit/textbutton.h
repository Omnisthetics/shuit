#ifndef TEXTBUTTON_H_
#define TEXTBUTTON_H_

#include "./image.h"
#include "./uiobject.h"
#include "./colourobject.h"

class Font;

class TextButton : public UiObject
{
private:
	Font * font;

	//Forced size members
	float forcedWidth;
	float forcedHeight;

	//Extra size members
	float extraWidth;
	float extraHeight;

	//Text members
	std::string text;
	glm::vec4 textColour;
	int textAlignment;

	//Icon members
	bool iconVisible;
	Image icon;
	float iconWidth;
	float iconHeight;
	float iconTextGap;

	//Padding members
	int paddingX;

	//Non-text colours
	ColourObject border;
	ColourObject background;
public:
	//Constructors
	TextButton() {};
	TextButton(std::string newText, Font * newFont, GLFWwindow * newWindow, int newHoriMode,
			int newVertMode, float newX, float newY);
	TextButton(const TextButton & other);
	TextButton & operator=(const TextButton & other);

	//Misc.
	void render(glm::mat4 projection, glm::mat4 view);
	void resize();

	//Getters
	std::string getText();
	int getContentWidth();

	//Setters
	void setForcedWidth(int w);
	void setForcedHeight(int h);
	void setExtraWidth(int extra);
	void setExtraHeight(int extra);
	void setText(std::string newText);
	void setTextColour(glm::vec4 colour);
	void setTextAlignment(int alignment);
	void setBorderColour(glm::vec4 colour);
	void setBackgroundColour(glm::vec4 colour);
	void setPadding(int x);
	void setIcon(Image newIcon);
	void setIcon(std::string iconFile, Shader * iconShader);
	void setIconWidth(int width);
	void setIconHeight(int height);
	void setIconTextGap(int gap);
	void setIconVisibility(bool vis);
};

#endif
