#include "./image.h"
#include <iostream>

//================================================================================================
//Constructors
//================================================================================================

Image::Image(std::string newPath, GLint newMinFilter, GLint newMagFilter, Shader * newShader)
{
	glTexMinFilter = newMinFilter;
	glTexMagFilter = newMagFilter;

	//Load image from file
	loadImage(newPath, glTexMinFilter, glTexMagFilter);

	//Assign shader
	shader = newShader;
}

Image::Image(const Image & image)
{
	texture = image.texture;
	filepath = image.filepath;
	VAO = image.VAO;
	VBO = image.VBO;
	EBO = image.EBO;
	shader = image.shader;
	glTexMinFilter = image.glTexMinFilter;
	glTexMagFilter = image.glTexMagFilter;
}

Image & Image::operator=(const Image & image)
{
	texture = image.texture;
	filepath = image.filepath;
	VAO = image.VAO;
	VBO = image.VBO;
	EBO = image.EBO;
	shader = image.shader;
	glTexMinFilter = image.glTexMinFilter;
	glTexMagFilter = image.glTexMagFilter;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

void Image::loadImage(std::string newPath, GLint glTexMinFilter, GLint glTexMagFilter)
{
	texture = Texture2D(newPath);
	texture.setParameteri(GL_TEXTURE_WRAP_S, GL_REPEAT);
	texture.setParameteri(GL_TEXTURE_WRAP_T, GL_REPEAT);
	texture.setParameteri(GL_TEXTURE_MIN_FILTER, glTexMinFilter);
	texture.setParameteri(GL_TEXTURE_MAG_FILTER, glTexMagFilter);

	float vertices[4][5] =
	{
		{-texture.getWidth()/2, -texture.getHeight()/2, 0.0f, 0.0f, 0.0f},
		{texture.getWidth()/2, -texture.getHeight()/2, 0.0f, 1.0f, 0.0f},
		{texture.getWidth()/2, texture.getHeight()/2, 0.0f, 1.0f, 1.0f},
		{-texture.getWidth()/2, texture.getHeight()/2, 0.0f, 0.0f, 1.0f}
	};

	int indices[6] = {0, 1, 2, 0, 2, 3};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	//Texture coordinate attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
}

void Image::render(glm::mat4 projection, glm::mat4 view, glm::mat4 model, float width, float height)
{
	texture.bind();

	if (width != 0 && height != 0)
	{
		float vertices[4][5] = {
			{-width/2, -height/2, 0.0f, 0.0f, 0.0f},
			{width/2, -height/2, 0.0f, 1.0f, 0.0f},
			{width/2, height/2, 0.0f, 1.0f, 1.0f},
			{-width/2, height/2, 0.0f, 0.0f, 1.0f}
		};

		//Update VBO
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	shader->use();
	shader->setInt("texChannels", texture.getChannels());
	shader->setMat4("model", model);
	shader->setMat4("view", view);
	shader->setMat4("projection", projection);

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void Image::renderClip(glm::mat4 projection, glm::mat4 view, glm::mat4 model, float width,
		float height, Rect clip)
{
	texture.bind();

	float texCoordX[4] = {
		clip.x/texture.getWidth(),
		(clip.x + clip.w)/texture.getWidth(),
		(clip.x + clip.w)/texture.getWidth(),
		clip.x/texture.getWidth(),
	};

	float texCoordY[4] = {
		1.0f - (clip.y + clip.h)/texture.getHeight(),
		1.0f - (clip.y + clip.h)/texture.getHeight(),
		1.0f - clip.y/texture.getHeight(),
		1.0f - clip.y/texture.getHeight(),
	};

	if (width != 0 && height != 0)
	{
		float vertices[4][5] = {
			{-width/2, -height/2, 0.0f, texCoordX[0], texCoordY[0]},
			{width/2, -height/2, 0.0f, texCoordX[1], texCoordY[1]},
			{width/2, height/2, 0.0f, texCoordX[2], texCoordY[2]},
			{-width/2, height/2, 0.0f, texCoordX[3], texCoordY[3]},
		};

		//Update VBO
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		float vertices[4][5] = {
			{-clip.w/2, -clip.h/2, 0.0f, texCoordX[0], texCoordY[0]},
			{clip.w/2, -clip.h/2, 0.0f, texCoordX[1], texCoordY[1]},
			{clip.w/2, clip.h/2, 0.0f, texCoordX[2], texCoordY[2]},
			{-clip.w/2, clip.h/2, 0.0f, texCoordX[3], texCoordY[3]},
		};

		//Update VBO
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	shader->use();
	shader->setInt("texChannels", texture.getChannels());
	shader->setMat4("model", model);
	shader->setMat4("view", view);
	shader->setMat4("projection", projection);

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

//================================================================================================
//Getters
//================================================================================================

int Image::getWidth()
{
	return texture.getWidth();
}

int Image::getHeight()
{
	return texture.getHeight();
}

Texture2D * Image::getTexture()
{
	return &texture;
}

std::string Image::getFilepath()
{
	return filepath;
}
