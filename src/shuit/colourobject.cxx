#include "./colourobject.h"

//================================================================================================
//Constructors
//================================================================================================

ColourObject::ColourObject(glm::vec4 newColour)
{
	colour = newColour;

	shader = Shader("./src/shaders/colourobject.vert", "./src/shaders/colourobject.frag");

	float vertices[4][3] =
	{
		{-0.5f, -0.5f, 0.0f},
		{0.5f, -0.5f, 0.0f},
		{0.5f, 0.5f, 0.0f},
		{-0.5f, 0.5f, 0.0f}
	};

	int indices[6] = {0, 1, 2, 0, 2, 3};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);
}

ColourObject::ColourObject(const ColourObject & other)
{
	VAO = other.VAO;
	VBO = other.VBO;
	EBO = other.EBO;
	shader = other.shader;
	colour = other.colour;
}

ColourObject & ColourObject::operator=(const ColourObject & other)
{
	VAO = other.VAO;
	VBO = other.VBO;
	EBO = other.EBO;
	shader = other.shader;
	colour = other.colour;

	return *this;
}

//================================================================================================
//Misc.
//================================================================================================

void ColourObject::render(glm::mat4 projection, glm::mat4 view, glm::mat4 model)
{
	shader.use();
	shader.setVec4("colour", colour);
	shader.setMat4("model", model);
	shader.setMat4("view", view);
	shader.setMat4("projection", projection);

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void ColourObject::renderLineLoop(glm::mat4 projection, glm::mat4 view, glm::mat4 model)
{
	shader.use();
	shader.setVec4("colour", colour);
	shader.setMat4("model", model);
	shader.setMat4("view", view);
	shader.setMat4("projection", projection);

	glBindVertexArray(VAO);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
}

//================================================================================================
//Misc.
//================================================================================================

glm::vec4 ColourObject::getColour()
{
	return colour;
}

//================================================================================================
//Misc.
//================================================================================================

void ColourObject::setColour(glm::vec4 newColour)
{
	colour = newColour;
}
