#OBJS specifies which files to compile as part of the project
OBJS = $(wildcard ./src/*.c) $(wildcard ./src/*.cxx)

#CC specifies which compiler we're using
CC = g++

#COMPILER_FLAGS specifies the additional compilation options we're using
# -w suppresses all warnings
COMPILER_FLAGS = -w -g -std=c++1z -O0 -pipe

#INCLUDE_FLAGS specifies folders and files we wish to include
INCLUDE_FLAGS = $(shell pkg-config freetype2 --cflags)

#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS = -lpthread -lglfw -lGL -lGLU -ldl -lstdc++fs -lfreetype

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = shuit-test

#Default compilation target
all: $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) $(INCLUDE_FLAGS) -o $(OBJ_NAME)

clean:
	rm ./shuit-test
